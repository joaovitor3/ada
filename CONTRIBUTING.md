# Contributing guidelines

Thank you for taking the time to contribute! :tada:

## What should I know before I get started?

Please make sure to read our [READ_ME](https://github.com/fga-eps-mds/2019.1-Grupo-3/blob/master/README.md) in order to get to know more about our project, system requirements and how to build the system.

Make sure to read our [Code of conduct](https://github.com/fga-eps-mds/2019.1-Grupo-3/blob/master/CODE_OF_CONDUCT.md) as well.

## How Can I Contribute?

### Reporting Bugs

Before creating a bug report, please refer to the list of open issues, someone may have already reported it.
When creating a bug report, please provide as much information as possible:

* Your title should be clear and descriptive
* Describe the steps to reproduce the bug
* Tell us what was expected and what happened that didn't go as such
* Include any images or screenshots that could help to identify the problem
* If the problem isn't related to any specific action, explain what you were doing at the time it happened
* Say which OS you are using
* Say which version of the system you encountered this bug

### Suggesting Enhancements

Before suggesting an enhancement, please refer to the list of open issues, someone may have already suggested it.
When suggesting an enhancement, please provide as much information as possible:

* Your title should be clear and descriptive
* Describe how the enhancement would work step by step and how it would take place
* Provide examples
* Include any images or screenshots that could help elucidate the enhancement
* Explain why the enhancement would be useful
* Say which OS you are using
* Say which version of the system you encountered this bug

## Templates

In order to make any contribution, please refer to our templates:

* [Issue template](https://github.com/fga-eps-mds/2019.1-Grupo-3/blob/master/.github/ISSUE_TEMPLATE/issue-template.md)
* [Pull request template](https://github.com/fga-eps-mds/2019.1-Grupo-3/blob/master/pull_request_template.md)
* [Template for commits](https://github.com/fga-eps-mds/2019.1-Grupo-3/)
